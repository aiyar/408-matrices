#include <stdio.h>
#include <stdlib.h>
#include "matrixADT.h"

template <class T>
matrixADT<T>::matrixADT(unsigned int n, unsigned int m) {
    this->row_size = n;
    this->col_size = m;
    allocate_matrix();
}   

template <class T>
matrixADT<T>::matrixADT(unsigned int n) {
    this->row_size = n;
    this->col_size = n;
    allocate_matrix();
} 

template <class T>
matrixADT<T>::matrixADT() {
    this->allocated = false;
}

template<class T>
matrixADT<T>::matrixADT(const matrixADT& src) {
    copy(src);
}

template <class T>
void matrixADT<T>::copy(const matrixADT& src) {
    this->row_size = src.row_size;
    this->col_size = src.col_size;
    allocate_matrix();
    for (int i = 0; i < row_size; ++i) {
        for (int j = 0; j < col_size; ++j) {
            matrix[i][j] = src.matrix[i][j];
        }
    }
}

template <class T>
matrixADT<T>::~matrixADT() {
    if (allocated) {
        for (int i = 0; i < row_size; ++i) {
            delete [] matrix[i];
        }
        delete [] matrix;
    }
}

template <class T>
void matrixADT<T>::allocate_matrix() {
    allocated = true;
    matrix = new T* [row_size];
    for (int i = 0; i < row_size; ++i) {
        *(matrix + i) = new T[col_size];
    }
    set(0);
}

template <class T>
void matrixADT<T>::set(T v) {
    for (int i = 0; i < row_size; ++i) {
        for (int j = 0; j < col_size; ++j) {
            matrix[i][j] = v;
        } 
    }
}

template <class T>
void matrixADT<T>::set(T* vals, unsigned int size) {
    
    if ((row_size * col_size) != size) {
        printf("Cannot set to %d, invalid size\n", size);
    }
    else {
        for (int i = 0; i < row_size; ++i) {
            for (int j = 0; j < col_size; ++j) {
                matrix[i][j] = vals[row_size*i+j];
            }
        }
    }
}

template <class T>
matrixADT<T> matrixADT<T>::add(const matrixADT& a, const matrixADT& b) {
    matrixADT<T> c(a.row_size, a.col_size);

    for (int i = 0; i < row_size; ++i) {
        for (int j = 0; j < col_size; ++j) {
            c.matrix[i][j] = a.matrix[i][j] + b.matrix[i][j];
        }
    }

    return c;
}

template <class T>
matrixADT<T> matrixADT<T>::sub(const matrixADT& a, const matrixADT& b) {
    matrixADT<T> c(a.row_size, a.col_size);

    for (int i = 0; i < row_size; ++i) {
        for (int j = 0; j < col_size; ++j) {
            c.matrix[i][j] = a.matrix[i][j] - b.matrix[i][j];
        }
    }

    return c;
}

template <class T>
matrixADT<T> matrixADT<T>::mul(const matrixADT& a, const matrixADT& b) {
    matrixADT<T> c(a.row_size, a.col_size);

    for (int i = 0; i < row_size; ++i) {
        for (int j = 0; j < col_size; ++j) {
            for (int k = 0; k < row_size; ++k) {
                c.matrix[i][j] += a.matrix[i][k] * b.matrix[k][j]; 
            }
        }
    }

    return c;
}

template <class T>
void matrixADT<T>::print() {
    for (int i = 0; i < row_size; ++i) {
        for (int j = 0; j < col_size; ++j) {
            printf("[%.2f]", matrix[i][j]);
        } 
        printf("\n");
    }
    printf("\n");
}

template <class T>
matrixADT<T>& matrixADT<T>::operator= (const matrixADT& src) {
    if (this == &src) {
        return *this;
    }
    this->~matrixADT();
    copy(src);
    return *this;
}

template <class V>
matrixADT<V> operator+(matrixADT<V>& a, matrixADT<V>& b) {
    return a.add(a,b);
}

template <class V>
matrixADT<V> operator-(matrixADT<V>& a, matrixADT<V>& b) {
    return a.sub(a,b);
}

template <class V>
matrixADT<V> operator*(matrixADT<V>& a, matrixADT<V>& b) {
    return a.mul(a,b);
}

int main() {

    matrixADT<double> a(5);
    matrixADT<double> b(5);

    double val1[] = {1,2,3,4,5,2,2,2,2,2,3,1,1,1,3,0,0,2,3,-2,4,4,-4,0,0};
    a.set(val1,25);
    printf("M1:\n");
    a.print();

    double val2[] = {1,0,0,0,0,1,2,1,2,1,0,0,1,0,0,1,1,1,1,1,2,2,-2,2,2};
    b.set(val2,25);
    printf("M2:\n");
    b.print();

    matrixADT<double> c;
    c = a + b;
    printf("M3:\n");
    c.print();

    c = a - b;
    printf("M4:\n");
    c.print();

    c = a * b;
    printf("M5:\n");
    c.print();

}
