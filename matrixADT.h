#pragma once
template <class T> 
class matrixADT {
    private:
        T** matrix;
        unsigned int row_size;
        unsigned int col_size;
        bool allocated;
        
        void allocate_matrix();
        void copy(const matrixADT&);
    public:
        matrixADT();
        matrixADT(unsigned int, unsigned int);
        matrixADT(unsigned int);
        matrixADT(const matrixADT&);
        ~matrixADT();
        void set(T);
        void set(T*, unsigned int);
        void print();
        matrixADT add(const matrixADT&, const matrixADT&);
        matrixADT sub(const matrixADT&, const matrixADT&);
        matrixADT mul(const matrixADT&, const matrixADT&);

        matrixADT& operator= (const matrixADT&);

        template <class V>
        friend matrixADT<V> operator+(matrixADT<V>&, matrixADT<V>&);

        template <class V>
        friend matrixADT<V> operator-(matrixADT<V>&, matrixADT<V>&);
        
        template <class V>
        friend matrixADT<V> operator*(matrixADT<V>&, matrixADT<V>&);
};
