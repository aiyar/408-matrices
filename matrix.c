// Using C to underscore the point that
// no classes are being used

#include <stdio.h>
#include <stdlib.h>

// Prototypes
double** allocate_matrix(int n);
void free_matrix(double** m, int n);
void print_matrix(double** m, int n);
double** add(double** a, double** b, int n); 
double** subtract(double** a, double** b, int n);
double** mult(double** a, double** b, int n); 
void set_matrix(double** mat, double* vals, int size); 

// Vars
double** matrix1;
double** matrix2;

int i, j, k;

int main (int argc, char ** argv) {

    int size = 5;
    matrix1 = allocate_matrix(size);
    matrix2 = allocate_matrix(size);
    
    double val1[] = {1,2,3,4,5,2,2,2,2,2,3,1,1,1,3,0,0,2,3,-2,4,4,-4,0,0};
    set_matrix(matrix1,val1,size);
    printf("M1:\n");
    print_matrix(matrix1,size);

    double val2[] = {1,0,0,0,0,1,2,1,2,1,0,0,1,0,0,1,1,1,1,1,2,2,-2,2,2};
    set_matrix(matrix2,val2,size);
    printf("\nM2:\n");
    print_matrix(matrix2,size);

    double** c;
    c = add(matrix1,matrix2,size);
    printf("\nM3:\n");
    print_matrix(c,size);

    free_matrix(c,size);
    c = subtract(matrix1,matrix2,size);
    printf("\nM4:\n");
    print_matrix(c,size);

    free_matrix(c,size);
    c = mult(matrix1,matrix2,size);
    printf("\nM5:\n");
    print_matrix(c,size);
    
    free_matrix(c,size);
    free_matrix(matrix1,size);
    free_matrix(matrix2,size);
}

// Functions
void print_matrix(double** m, int n) {
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            printf("[%.2f]", m[i][j]);
        }
        printf("\n");
    }
}

void set_matrix(double** mat, double* vals, int size) {
    for (i = 0; i < size; ++i) {
        for (j = 0; j < size; ++j) {
            mat[i][j] = vals[size*i+j]; 
        } 
    } 
}

double** add(double** a, double** b, int n) {
    double** c = allocate_matrix(n);
    for(i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            c[i][j] = a[i][j] + b[i][j];
        }
    }
    return c;
}

double** subtract(double** a, double** b, int n) {
    double** c = allocate_matrix(n);
    for(i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            c[i][j] = a[i][j] - b[i][j];
        }
    }
    return c;
}

double** mult(double** a, double** b, int n) {
    double** c = allocate_matrix(n);
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            for (k = 0; k < n; ++k) {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
    return c;
}

double** allocate_matrix(int n) {
    double** mat;
    mat = (double**) malloc(n * sizeof(double*));
    for (i = 0; i < n; ++i) {
        *(mat + i) = (double*) (malloc(n * sizeof(double)));
    }
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            *(*(mat + i)+j) = 0;
        }
    }
    return mat;
}

void free_matrix(double** m, int n) {
    for (i = 0; i < n; ++i) {
        free(m[i]);
    }
    free(m);
}
